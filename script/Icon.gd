extends TextureRect


signal imma_head_out


enum { UP, DOWN } # direction constants
enum { INCREASE, DECREASE, IDLE } # speed constatns

var column_h: float # выоста колонки (равна высоте доски)
var death_offset: float # дистанция, после которой иконка удаляется
var offset_y: float # вертикальный отступ между иконками
var speed: float = 0 # скорость
var max_speed: int # максимальная скорость
var direction: int # направление движения
var speed_status: int = IDLE # ускорение или замедление

var speed_inc: float = 103.0 # шаг увеличения скорости
var speed_dec: float = 53.0 # шаг уменьшения скорости

var blur: bool = false # состояние блюра


# BUILTINS - - - - - - - - -


func _process(delta: float) -> void:
	# если запустили кручение
	if speed > 0 and speed_status != IDLE:
		# если разгоняется
		if speed_status == INCREASE:
			if speed < max_speed:
				speed += speed_inc
			elif speed > max_speed:
				speed = max_speed
				if not blur:
					set_blur(true)
		# если останавливается
		elif speed_status == DECREASE:
			speed -= speed_dec
			if speed < 0:
				speed_status = IDLE
				speed = 0
				if blur:
					set_blur(false)
		# если крутится вверх
		if direction == UP:
			if self.rect_position.y <= death_offset:
				emit_signal("imma_head_out", speed, self.rect_position.y)
				queue_free()
			else:
				self.rect_position.y -= round(speed * delta)
		# если крутится вниз
		elif direction == DOWN:
			if self.rect_position.y >= death_offset:
				emit_signal("imma_head_out", speed, self.rect_position.y)
				queue_free()
			else:
				self.rect_position.y += round(speed * delta)


# METHODS - - - - - - - - -


# начала кручения. запускается из столбца
func move(new_speed: float) -> void:
	speed_status = INCREASE
	speed = new_speed


# конец кручения. запускается из столбца
func slow_down(new_speed: float) -> void:
	speed_status = DECREASE
	speed = new_speed


# нормализация позиции иконок
func normalize_position(offset: float) -> void:
	var _t: int
	_t = ($Tween as Tween).interpolate_property(self, "rect_position:y", rect_position.y, rect_position.y - offset, 0.2)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка блюра иконки
func set_blur(param: bool) -> void:
	blur = param
	material.set_shader_param("blur", param)


# SIGNALS - - - - - - - - -
