extends Control


const Column: PackedScene = preload("res://scene/Column.tscn")


signal score_increased
signal spinnig_changed
signal auto_spinnig_changed
signal autospin_count_changed


enum { UP, DOWN }


const BOARDS: Array = [
	preload("res://assets/image/board_lvl_1.png"),
	null,
	preload("res://assets/image/board_lvl_3.png"),
]

const PROPS: = [
	{
		w = 192,
		h = 170,
		count_x = 4, # количество по горизонтали
		count_y = 3, # количество по вертикали
		offset_x = 4, # отступ между иконками
		offset_y = 5,
		variations = 8, # вариации иконок
		board_offset_x = 0, # маржин доски
		board_offset_y = -30,
		board_bg_offset_x = 0, # отступ картинки доски
		board_bg_offset_y = -25,
	},
	{
		w = 150,
		h = 150,
		count_x = 4,
		count_y = 3,
		offset_x = 10,
		offset_y = 10,
		variations = 7,
		board_offset_x = 0,
		board_offset_y = -25,
		board_bg_offset_x = 0,
		board_bg_offset_y = -25,
	},
	{
		w = 180,
		h = 165,
		count_x = 5,
		count_y = 3,
		offset_x = 4,
		offset_y = 13,
		variations = 8,
		board_offset_x = 0,
		board_offset_y = -31,
		board_bg_offset_x = 0,
		board_bg_offset_y = -30,
	},
]
var textures: Array = [] # текстуры иконок всех уровней

var spinning: bool = false setget set_spinning, get_spinning # состояние спина
var auto_spinning: bool = false setget set_auto_spinning, get_auto_spinning # состояние автоспина
var auto_spin_max: int = 10 # количество повторов автоспина
var auto_spin_count: int = 1 # счетчик количества повторов автоспина


# BUILTINS - - - - - - - - -


func _ready() -> void:
	# подгрузка текстур иконок
	for i in PROPS.size():
		textures.append([])
		for j in PROPS[i].variations:
			var icon: Texture = load("res://assets/image/ic_lvl_%s/%s.png" % [i + 1, j + 1])
			textures[i].append(icon)
	emit_signal("autospin_count_changed", auto_spin_count, auto_spin_max)


# METHODS - - - - - - - - -


# setget spinning
func set_spinning(status: bool) -> void:
	spinning = status
	emit_signal("spinnig_changed", status)

func get_spinning() -> bool:
	return spinning


# setget auto_spinning
func set_auto_spinning(status: bool) -> void:
	auto_spinning = status
	emit_signal("auto_spinnig_changed", status)

func get_auto_spinning() -> bool:
	return auto_spinning


# загрузка уровня
func prepare_level(level: int) -> void:
	($Bg as TextureRect).texture = load("res://assets/image/bg_lvl_%s.png" % level) as Texture
	($BoardBg as TextureRect).texture = BOARDS[level - 1] as Texture
	($BoardBg as TextureRect).set_size(Vector2.ZERO)
	var prop: Dictionary = PROPS[level - 1]
	var board_w: float = prop.count_x * prop.w + prop.offset_x * (prop.count_x - 1)
	var board_h: float = prop.count_y * prop.h + prop.offset_y * (prop.count_y - 1)
	var offset: Vector2 = (self.get_rect().size - Vector2(board_w, board_h)) / 2 + Vector2(prop.board_offset_x, prop.board_offset_y)
	var offset_bg: Vector2 = (self.get_rect().size - ($BoardBg as TextureRect).get_rect().size) / 2 + Vector2(prop.board_bg_offset_x, prop.board_bg_offset_y)
	($Board as Control).set_size(Vector2(board_w, board_h))
	($Board as Control).set_position(offset)
	($BoardBg as TextureRect).set_position(offset_bg)
	for i in prop.count_x:
		var column: Control = Column.instance() as Control
		column.props = prop
		column.textures = textures[level - 1]
		# чтобы в разные стороны крутилось - i % 2
		if i == 0 or i == prop.count_x - 1:
			column.direction = UP
		else:
			column.direction = DOWN
		column.set_position(Vector2((prop.w + prop.offset_x) * i, 0.0))
		column.set_size(Vector2(prop.w, board_h))
		($Board as Control).add_child(column)


# очистка уровня
func clear_level() -> void:
	for i in ($Board as Control).get_children():
		if not i is TextureRect:
			i.queue_free()
	($BoardBg as TextureRect).texture = null


# кручение слотов
func spin() -> void:
	if not get_spinning():
		set_spinning(true)
		var j: float = 0.0
		for i in ($Board as Control).get_children():
			if not i is TextureRect:
				match j:
					1.0, 3.0:
						i.move_icons(0.3)
					2.0, 0.0, 4.0:
						i.move_icons(0.0)
				j += 1.0
		yield(get_tree().create_timer(3), "timeout")
		j = 0.0
		for i in ($Board as Control).get_children():
			if not i is TextureRect:
				match j:
					0.0, 4.0:
						i.stop_icons(0.0)
					1.0, 3.0:
						i.stop_icons(0.2)
					2.0:
						i.stop_icons(0.4)
				j += 1.0
		yield(get_tree().create_timer(1.5), "timeout")
		emit_signal("score_increased")
		_spin_stop()


# вызов начала автоспина
func auto_spin() -> void:
	if not get_spinning() and not get_auto_spinning():
		set_auto_spinning(true)
		spin()


# остановка спина. повторяется
func _spin_stop() -> void:
	set_spinning(false)
	if get_auto_spinning():
		yield(get_tree().create_timer(1.5), "timeout")
		_update_autospin_count()


# сигнал таймера паузы. перезапускает спин автоматически
func _update_autospin_count() -> void:
	if auto_spin_max > auto_spin_count: # если меньше максимального количества спинов, то продолжаем крутить
		auto_spin_count += 1 # увеличиваем счетчик
		spin()
	else: # если закончили, то останавливаем автоспины
		set_spinning(false)
		set_auto_spinning(false)
		auto_spin_count = 1
	emit_signal("autospin_count_changed", auto_spin_count, auto_spin_max)


# SIGNALS - - - - - - - - -
