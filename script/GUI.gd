extends Control


const Counter: PackedScene = preload("res://scene/Counter.tscn")


signal btn_back_pressed
signal btn_spin_pressed
signal btn_autospin_pressed


# меняется сигналами из GameSlot через Main
var spinning: bool = false
var auto_spinning: bool = false


var score: float = 10000.0
var score_win = 0.0
var level: int = 1 # уровень на шкале уровня
var level_score: float = 0.0 # выигранные очки
var level_step: int = 200 # шаг уровня
var chanse_to_win: int = 15 # шанс на победу. условно в процентах
var rate: float = 15.0 # повышающий коэффициент при выигрыше

var min_bet: float = 0.25 # минимальный порог ставки
var max_bet: float = 5.0 # максимальный порог ставки
var bet_step: float= 0.25 # шаг инкремента/дикремента
var bet: float= 2.5 # текущий коэффициент ставки


# BUILTINS - - - - - - - - -


func _ready() -> void:
	set_level(0)


func _process(_delta: float) -> void:
	($Header/Score/Label as Label).text = "$ %s" % str(round(score * 100) / 100)
	($Header/Level/Label as Label).text = "$ %s/%s" % [round(level_score), round(level * level_step)]
	($Footer/Bet/Label as Label).text = "$ %s" % str(round(bet * 100) / 100)
	($Footer/BgWin/ScoreWin as Label).text = "$ %s" % str(round(score_win * 100) / 100)


# METHODS - - - - - - - - -


# отображение контролов
func show_hide_controls(show: bool) -> void:
	var _t: int
	var back_pos: int = 6
	var footer_pos: float = get_rect().size.y - ($Footer as Control).get_rect().size.y
	if show:
		_t = ($Tween as Tween).interpolate_property($Header/BtnBack, "rect_position:y", -100.0, back_pos, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer, "rect_position:y", footer_pos + 500.0, footer_pos, 0.3)
	else:
		_t = ($Tween as Tween).interpolate_property($Header/BtnBack, "rect_position:y", back_pos, -100.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer, "rect_position:y", footer_pos, footer_pos + 500.0, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка очков
func set_score() -> void:
	var _t: int
	var counter: Label = Counter.instance() as Label
	if randi() % 100 <= chanse_to_win + chanse_to_win * bet / 2:
		var count: int = floor(rate * bet + level) as int
		_t = ($Tween as Tween).interpolate_property(self, "score", score, score + count, 0.5)
		_t = ($Tween as Tween).interpolate_property(self, "score_win", score_win, score_win + count, 0.5)
		set_level(count)
		counter.set("increase", true)
		counter.set("count", count)
	else:
		_t = ($Tween as Tween).interpolate_property(self, "score", score, score - bet, 0.5)
		counter.set("increase", false)
		counter.set("count", bet)
	get_parent().add_child(counter)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func set_gift_score() -> void:
	var _t: int
	var counter: Label = Counter.instance() as Label
	var count: int = 30000
	_t = ($Tween as Tween).interpolate_property(self, "score", score, score + count, 0.5)
	counter.set("increase", true)
	counter.set("count", count)
	get_parent().add_child(counter)
	_t = ($Tween as Tween).interpolate_property($Header/BtnGift, "rect_position:y", ($Header/BtnGift as Button).rect_position.y, -100, 0.4, Tween.TRANS_CUBIC)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# установка уровня
func set_level(count: int) -> void:
	var _t: int
	var max_width: int = 250
	level_score = level_score + float(count)
	if level_score > level * level_step:
		level_score = level_score - level * level_step
		level += 1
	($Header/Level/IcLevel/Label as Label).text = "%s" % level
	var progress: float = lerp($Header/Level/ProgressBg/ProgressFg.rect_min_size.x, max_width, level_score / (level * level_step))
	_t = ($Tween as Tween).interpolate_property($Header/Level/ProgressBg/ProgressFg, "rect_size:x", ($Header/Level/ProgressBg/ProgressFg as NinePatchRect).rect_size.x, progress, 0.2)
	_t = ($Tween as Tween).interpolate_property(self, "level_score", level_score - count, level_score, 0.2)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение автоспина
func show_hide_autospin_progress(show: bool) -> void:
	var _t: int
	var progress_pos: float = (($Footer as Control).get_rect().size.y - ($Footer/AutoSpinProgress as Control).get_rect().size.y) / 2
	if show:
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "rect_position:y", 200, progress_pos, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "rect_position:y", 36, 200, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "rect_position:y", 18, 200, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "rect_position:y", 18, 200, 0.3)
	else:
		_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress, "rect_position:y", progress_pos, 200, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet, "rect_position:y", 200, 36, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnAutoSpin, "rect_position:y", 200, 18, 0.3)
		_t = ($Tween as Tween).interpolate_property($Footer/BtnSpin, "rect_position:y", 200, 18, 0.3)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	($Footer/AutoSpinProgress/ProgressBg/ProgressFg as NinePatchRect).rect_size.x = ($Footer/AutoSpinProgress/ProgressBg/ProgressFg as NinePatchRect).rect_min_size.x


func change_autospin_count(auto_spin_count: int, auto_spin_max: int) -> void:
	var _t: int
	var max_width: int = 500
	($Footer/AutoSpinProgress/ProgressBg/Label as Label).text = "%s / %s" % [auto_spin_count, auto_spin_max]
	var progress: float = lerp($Footer/AutoSpinProgress/ProgressBg/ProgressFg.rect_min_size.x, max_width, float(auto_spin_count) / auto_spin_max)
	var x: float = ($Footer/AutoSpinProgress/ProgressBg/ProgressFg as NinePatchRect).rect_size.x
	_t = ($Tween as Tween).interpolate_property($Footer/AutoSpinProgress/ProgressBg/ProgressFg, "rect_size:x", x, progress, 0.2)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# SIGNALS - - - - - - - - -


func _on_BtnBack_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_back_pressed")


func _on_BtnSpin_button_down() -> void:
	($TimerSpinLongPress as Timer).start()


func _on_BtnSpin_button_up() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_spin_pressed")
		($TimerSpinLongPress as Timer).stop()


func _on_Timer_timeout() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)


func _on_BtnMinus_pressed() -> void:
	var _t: int
	if not spinning and not auto_spinning:
		var old_bet: float = bet
		bet = bet - bet_step if bet > min_bet else min_bet
		_t = ($Tween as Tween).interpolate_property(self, "bet", old_bet, bet, 0.05)

		var btn_min_opacity: float = ($Footer/Bet/BtnMinus as Button).modulate.a
		var btn_max_opacity: float = ($Footer/Bet/BtnPlus as Button).modulate.a
		if bet == min_bet:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 0.0, 0.2)
		else:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.2)

		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func _on_BtnPlus_pressed() -> void:
	var _t: int
	if not spinning and not auto_spinning:
		var old_bet: float = bet
		bet = bet + bet_step if bet < max_bet else max_bet
		_t = ($Tween as Tween).interpolate_property(self, "bet", old_bet, bet, 0.1)

		var btn_max_opacity: float = ($Footer/Bet/BtnPlus as Button).modulate.a
		var btn_min_opacity: float = ($Footer/Bet/BtnMinus as Button).modulate.a
		if bet == max_bet:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, 0.2)
		else:
			_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", btn_max_opacity, 1.0, 0.2)
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, 0.2)

		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func _on_BtnAutoSpin_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)


func _on_BtnGift_pressed() -> void:
	set_gift_score()
